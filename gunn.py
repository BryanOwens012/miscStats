import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

data = 'data/gunn17.csv'
df = pd.read_csv(data)
# '————' is for Ronald Cushing, who died before graduation and is thus excluded from the dataset
df = df[df['Undergrad'] != '————']
count_students = df.shape[0]

def printCounts(df, dropna = True, normalize = False):
    counts = df['Undergrad']
    counts = counts.value_counts(dropna = dropna, normalize = normalize)
    hasnull = counts.isnull().any()
    count_colleges = counts.shape[0] - (1 if hasnull else 0)
    print(counts.to_string())
    print(f'\tTOTAL: {count_colleges} colleges enrolled {df.shape[0]} students ({round(df.shape[0] * 100 / count_students, 2)}% of student body)')

def plotCounts(df, num_colleges = 30):
    undergrad = df['Undergrad']
    num_counted = undergrad[:num_colleges].sum()
    undergrad[:num_colleges].plot(kind = 'bar')
    plt.suptitle(f'The {num_colleges} most popular colleges for the Gunn class of 2017')
    plt.title(f'(these comprise {num_counted} students out of {df.shape[0]})')
    plt.xlabel('College name')
    plt.ylabel('Count')
    plt.show()

def standardize(df):
    df = df.copy()
    special_programs = {'UPenn (Wharton)': 'UPenn',
                        'UPenn (SEAS)': 'UPenn',
                        'UPenn (SAS)': 'UPenn',
                        'CMU (SCS)': 'CMU',
                        'CMU (Tepper)': 'CMU',
                        'UC Berkeley (MET)': 'UC Berkeley',
                        'NYU (Tisch)': 'NYU',
                        'NYU (Stern)': 'NYU',
                        'USC (School of Dramatic Arts)': 'USC',
                        'USC (Kaufman)': 'USC',
                        'BostonU (Questrom)': 'BostonU',
                        'JTS-Columbia': 'Columbia',
                        'Northwestern (NUPSP)': 'Northwestern'}
    gap_years = {'Gap year -> Duke': 'Duke',
                 'Gap year -> Oxford': 'Oxford',
                 'Gap year -> SCU': 'SCU'}
    for key, value in special_programs.items():
        df['Undergrad'] = df['Undergrad'].replace(to_replace = key, value = value)
    for key, value in gap_years.items():
        df['Undergrad'] = df['Undergrad'].replace(to_replace = key, value = value)

    return df

ranks = {}
ranks['Stanford'] = ['Stanford']
ranks['MIT'] = ['MIT']
ranks['HYP'] = ['Harvard', 'Yale', 'Princeton']
ranks['HYPS'] = ranks['HYP'] + ranks['Stanford']
ranks['T5'] = ranks['HYPS'] + ranks['MIT'] # same as HYPSM
ranks['T10'] = ranks['T5'] + ['UChicago', 'Columbia', 'UPenn', 'Duke', 'Caltech']
ranks['T20'] = ranks['T10'] + ['Dartmouth', 'JHU', 'Northwestern', 'Brown', 'Cornell', 'Rice',
                               'Vanderbilt', 'Notre Dame', 'WashU', 'Georgetown']
ranks['T25'] = ranks['T20'] + ['Emory', 'UC Berkeley', 'UCLA', 'USC', 'CMU']

def printRankCounts(df, rank = 'T5'):
    df = df.copy()
    df = df.loc[df['Undergrad'].isin(ranks[rank]), :]
    printCounts(df)

def printRankCountsAll(df, ranks):
    for key, value in ranks.items():
        print('==========\n')
        print(f'{key} ---------')
        printRankCounts(df, key)

df = standardize(df)
printRankCountsAll(df, ranks)
